"use client";

import {
  Typography,
  Card,
  CardBody,
  IconButton,
} from "@material-tailwind/react";
import { EnvelopeIcon, PhoneIcon } from "@heroicons/react/24/solid";

export function ContactForm() {
  return (
    <section id="contact" className="px-8 py-28">
      <div>
        {/* @ts-ignore */}
        <Card shadow={true} className="container mx-auto border border-gray/50">
          {/* @ts-ignore */}
          <CardBody className="flex flex-col md:flex-row justify-between">
            <div className="mx-10 my-10 text-start">
              {/* @ts-ignore */}
              <Typography variant="h4" color="blue-gray" className="mb-4">
                Contact Me
              </Typography>
              {/* @ts-ignore */}
              <Typography variant="lead" className="w-full !text-gray-500">
                Ready to get started?. Feel free to reach out for
                collaborations, inquiries, or if you&apos;d like to discuss how
                I can contribute to your team. I look forward to connecting!
              </Typography>
            </div>
            <div className="w-full col-span-3 rounded-lg h-full py-8 p-10 md:p-16 bg-gray-900">
              {/* @ts-ignore */}
              <Typography variant="h4" color="white" className="mb-2">
                Contact Information
              </Typography>
              {/* @ts-ignore */}
              <Typography
                variant="lead"
                className="mx-auto mb-8 text-base !text-gray-500"
              >
                Contact me via email or phone for inquiries and opportunities
              </Typography>
              <div className="flex gap-5">
                <PhoneIcon className="h-6 w-6 text-white" />
                {/* @ts-ignore */}
                <Typography variant="h6" color="white" className="mb-2">
                  +91 7715030258 / 9326390669
                </Typography>
              </div>
              <div className="flex my-2 gap-5">
                <EnvelopeIcon className="h-6 w-6 text-white" />
                {/* @ts-ignore */}
                <Typography variant="h6" color="white" className="mb-2">
                  patilkirtish77@gmail.com
                </Typography>
              </div>
              <div className="flex items-center gap-5">
                {/* @ts-ignore */}
                <IconButton variant="text" color="white">
                  <i className="fa-brands fa-facebook text-lg" />
                </IconButton>
                {/* @ts-ignore */}
                <IconButton variant="text" color="white">
                  <i className="fa-brands fa-instagram text-lg" />
                </IconButton>
                {/* @ts-ignore */}
                <IconButton variant="text" color="white">
                  <i className="fa-brands fa-github text-lg" />
                </IconButton>
              </div>
            </div>
          </CardBody>
        </Card>
      </div>
    </section>
  );
}

export default ContactForm;

// <div className="w-full mt-8 md:mt-0 md:px-10 col-span-4 h-full p-5">
//   <form action="#">
//     <div className="mb-8 grid gap-4 lg:grid-cols-2">
//       {/* @ts-ignore */}
//       <Input
//         color="gray"
//         size="lg"
//         variant="static"
//         label="First Name"
//         name="first-name"
//         placeholder="eg. Lucas"
//         containerProps={{
//           className: "!min-w-full mb-3 md:mb-0",
//         }}
//       />
//       {/* @ts-ignore */}
//       <Input
//         color="gray"
//         size="lg"
//         variant="static"
//         label="Last Name"
//         name="last-name"
//         placeholder="eg. Jones"
//         containerProps={{
//           className: "!min-w-full",
//         }}
//       />
//     </div>
//     {/* @ts-ignore */}
//     <Input
//       color="gray"
//       size="lg"
//       variant="static"
//       label="Email"
//       name="first-name"
//       placeholder="eg. lucas@mail.com"
//       containerProps={{
//         className: "!min-w-full mb-8",
//       }}
//     />
//     {/* @ts-ignore */}
//     <Typography variant="lead" className="!text-blue-gray-500 text-sm mb-2">
//       What are you interested on?
//     </Typography>
//     <div className="-ml-3 mb-14 ">
//       {/* @ts-ignore */}
//       <Radio color="gray" name="type" label="Design" defaultChecked />
//       {/* @ts-ignore */}
//       <Radio color="gray" name="type" label="Development" />
//       {/* @ts-ignore */}
//       <Radio color="gray" name="type" label="Support" />
//       {/* @ts-ignore */}
//       <Radio color="gray" name="type" label="Other" />
//     </div>
//     {/* @ts-ignore */}
//     <Textarea
//       color="gray"
//       size="lg"
//       variant="static"
//       label="Your Message"
//       name="first-name"
//       containerProps={{
//         className: "!min-w-full mb-8",
//       }}
//     />
//     <div className="w-full flex justify-end">
//       {/* @ts-ignore */}
//       <Button className="w-full md:w-fit" color="gray" size="md">
//         Send message
//       </Button>
//     </div>
//   </form>
// </div>;

"use client";
import Image from "next/image";
import { Typography } from "@material-tailwind/react";

function Hero() {
  return (
    <header id="home" className="bg-white p-8">
      <div className="container mx-auto grid items-center my-4 md:my-10 lg:my-20  w-full grid-cols-1 lg:grid-cols-2 gap-10 lg:gap-0">
        <div className="row-start-2 lg:w-[36rem] mx-auto  lg:row-auto">
          {/* @ts-ignore */}
          <Typography
            variant="h1"
            color="blue-gray"
            className="mb-4 lg:text-5xl !leading-tight text-3xl"
          >
            Welcome to my Web <br /> Development Portofolio!
          </Typography>
          {/* @ts-ignore */}
          <Typography
            variant="lead"
            className="mb-4 !text-gray-500 md:pr-16 xl:pr-28"
          >
            I&apos;m <b className="text-gray-700 text-2xl">Kirtish Patil</b>, a
            passionate Full Stack developer based in India. Here, you&apos;ll
            get a glimpse of my journey in the world of web development, where
            creativity meets functionality.
          </Typography>
        </div>
        <Image
          width={1024}
          height={1024}
          alt="team work"
          src="/avatar/avatar2.png"
          className="md:size-[30rem] sm:size-[25rem]  lg:h-[36rem] lg:w-[36rem]  mx-auto rounded-full object-cover"
        />
      </div>
    </header>
  );
}

export default Hero;

"use client";

import { ProjectCard, ProjectCardProps } from "@/components";
import { Typography } from "@material-tailwind/react";

// const PROJECTS = [
//   {
//     img: "/image/blog-1.svg",
//     title: "App Orchestrator",
//     desc: "Mobile app designed to help users discover and explore local restaurants and cuisines.",
//   },
//   {
//     img: "/image/blog2.svg",
//     title: "Lead Management System",
//     desc: "Promotional landing page for a  fitness website Summer Campaign. Form development included.",
//   },
//   {
//     img: "/image/blog3.svg",
//     title: "Json Path Finder",
//     desc: "Mobile app designed to help users discover and explore local restaurants and cuisines.",
//   },
//   {
//     img: "/image/blog4.svg",
//     title: "Shoe Vista",
//     desc: "Ecommerce website offering  access to the latest and greatest gadgets and accessories.",
//   },
//   {
//     img: "/image/blog-1.svg",
//     title: "React Multi-branch Pipeline",
//     desc: "Mobile app designed to help users discover and explore local restaurants and cuisines.",
//   },
// ];
const PROJECTS: ProjectCardProps[] = [
  {
    img: "/projects/appOrch/1.png",
    title: "App Orchestrator",
    desc: (
      <p>
        A no-code low-code Platform that integrates different software solutions
        together.
      </p>
    ),
    isDemoAvailable: true,
    demo: "http://64.227.187.196:5005/login",
    isBlogAvailable: false,
    isCodeAvailable: false,
  },
  {
    img: "/projects/lms/1.png",
    title: "Lead Management System",
    desc: "An undergoing freelance project of Lead Management System for a fitness club.",
    isDemoAvailable: false,
    isBlogAvailable: false,
    isCodeAvailable: true,
    code: "https://gitlab.com/Kirtish_Patil/swlms",
  },
  {
    img: "/projects/pathFinder/1.png",
    title: "Json Path Finder",
    desc: "A Utility application to get x-path of a key in a json object",
    isDemoAvailable: true,
    demo: "http://64.227.187.196/",
    isBlogAvailable: false,
    isCodeAvailable: true,
    code: "https://gitlab.com/Kirtish_Patil/json-path-finder",
  },
  {
    img: "/projects/shoeVista/1.png",
    title: "Shoe Vista",
    desc: "Ecommerce website offering  access to the latest and greatest shoes and accessories.",
    isDemoAvailable: true,
    isBlogAvailable: false,
    isCodeAvailable: true,
    demo: "http://64.227.187.196:7000/",
    code: "https://gitlab.com/Kirtish_Patil/shoevista",
  },
  {
    img: "/projects/reactPipeline/1.jpg",
    title: "React Multi-branch Pipeline",
    desc: "A fully automated CI/CD pipeline for react application using Docker, Jenkins, and Ansible.",
    isDemoAvailable: false,
    isBlogAvailable: true,
    isCodeAvailable: true,
    code: "https://gitlab.com/Kirtish_Patil/whiteboard.git",
    blog: "/articles/creating-a-multibranch-cicd-pipeline-for-react",
  },
];

export function Projects() {
  return (
    <section id="projects" className="py-28 px-8">
      <div className="container mx-auto mb-20 text-center">
        {/* @ts-ignore */}
        <Typography variant="h2" color="blue-gray" className="mb-4">
          My Projects
        </Typography>
        {/* @ts-ignore */}
      </div>
      <div className="container mx-auto grid grid-cols-1 gap-x-10 gap-y-20 md:grid-cols-2 xl:grid-cols-3">
        {PROJECTS.map((props, idx) => (
          <ProjectCard key={idx} {...props} />
        ))}
      </div>
    </section>
  );
}

export default Projects;

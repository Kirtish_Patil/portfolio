"use client";

import { Typography } from "@material-tailwind/react";
import { SkillCard } from "@/components";
import ReactIcon from "@/components/icons/ReactIcon";
import NextjsIcon from "@/components/icons/NextjsIcon";
import TailwindIcon from "@/components/icons/TailwindIcon";
import NodejsIcon from "@/components/icons/NodejsIcon";
import MongodbIcon from "@/components/icons/MongodbIcon";
import TypescriptIcon from "@/components/icons/TypescriptIcon";
import JenkinsIcon from "@/components/icons/JenkinsIcon";
import DockerIcon from "@/components/icons/DockerIcon";
import AnsibleIcon from "@/components/icons/AnsibleIcon";
import LinuxLogo from "@/components/icons/LinuxLogo";
import { AiOutlineDotNet } from "react-icons/ai";
import DotnetIcon from "@/components/icons/DotnetIcon";

console.log("Typeof icon: ", typeof ReactIcon);

const SKILLS = [
  {
    icons: [
      { icon: ReactIcon, toolTip: "React" },
      { icon: NextjsIcon, toolTip: "Nextjs" },
      { icon: TailwindIcon, toolTip: "Tailwind" },
    ],
    title: "Frontend Development:",
    children:
      "Creating beautiful and functional web experiences is my forte. Using the latest technologies and best practices, I design and build applications that captivate and engage users.",
  },
  {
    icons: [
      {
        icon: NodejsIcon,
        toolTip: "Nodejs",
      },
      {
        icon: TypescriptIcon,
        toolTip: "Typescript",
      },
      {
        icon: MongodbIcon,
        toolTip: "MongoDB",
      },
      {
        icon: DotnetIcon,
        toolTip: ".Net Core",
      },
    ],
    title: "Backend Development",
    children:
      "I specialize in developing production-ready backend systems using Node.js and TypeScript, ensuring they are both maintainable and scalable. I employ industry best practices, including a robust logging system and comprehensive error handling, to enhance reliability and facilitate troubleshooting.",
  },
  {
    icons: [
      {
        icon: DockerIcon,
        toolTip: "Docker",
      },
      {
        icon: JenkinsIcon,
        toolTip: "Jenkins",
      },
      {
        icon: AnsibleIcon,
        toolTip: "Ansible",
      },
      {
        icon: LinuxLogo,
        toolTip: "Linux",
      },
    ],
    title: "Devops",
    children:
      "In addition to development, I manage the seamless deployment of applications using technologies such as Docker, Jenkins, and Ansible. I leverage version control with Git and integrate these tools within a Linux environment to ensure efficient and reliable deployment processes.",
  },
  // {
  //   icon: DocumentTextIcon,
  //   title: "Testing and Documentation",
  //   children:
  //     "I am committed to rigorously testing applications by writing comprehensive unit test cases and maintaining a detailed issue tracker to ensure that no problems go unresolved. Additionally, I prioritize documentation, enabling new developers to onboard quickly and work on the project with ease.",
  // },
];

export function Skills() {
  return (
    <section id="skills" className="px-8 py-28">
      <div className="container mx-auto mb-20 text-center">
        {/* @ts-ignore */}
        <Typography
          variant="h3"
          color="blue-gray"
          className="mb-2 font-bold uppercase"
        >
          my skills
        </Typography>
        {/* @ts-ignore */}
        <Typography variant="h1" color="blue-gray" className="mb-4">
          What I do
        </Typography>
        {/* @ts-ignore */}
        <Typography
          variant="lead"
          className="mx-auto w-full !text-gray-500 lg:w-10/12"
        >
          I&apos;m a web developer who crafts high-performing, efficient web
          applications, seamlessly guiding them from initial code to automated
          deployment, all while embracing DevOps philosophies for optimal
          efficiency. <br /> Discover below how I can help you.
        </Typography>
      </div>
      <div className="container mx-auto justify-items-center items-start grid grid-cols-1 gap-y-10 md:grid-cols-2 lg:grid-cols-3 ">
        {SKILLS.map((props, idx) => (
          <div key={idx} className="flex justify-center h-full">
            <SkillCard key={idx} {...props} />
          </div>
        ))}
      </div>
    </section>
  );
}

export default Skills;

"use client";
import Image from "next/image";
import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Button,
} from "@material-tailwind/react";
import { useRouter, redirect } from "next/navigation";
import Link from "next/link";
import { ReactNode } from "react";

export interface ProjectCardProps {
  img: string;
  title: string;
  desc: ReactNode;
  isDemoAvailable: boolean;
  isBlogAvailable: boolean;
  isCodeAvailable: boolean;
  demo?: string;
  blog?: string;
  code?: string;
}

export function ProjectCard({
  img,
  title,
  desc,
  isDemoAvailable,
  code,
  isBlogAvailable,
  isCodeAvailable,
  blog,
  demo,
}: ProjectCardProps) {
  const router = useRouter();
  return (
    // @ts-ignore
    <Card color="transparent" shadow={false} className="">
      {/* @ts-ignore */}
      <CardHeader floated={false} className="mx-0 mt-0 mb-6 h-56">
        <Image
          src={img}
          alt={title}
          width={768}
          height={768}
          className="h-full w-full object-cover"
        />
      </CardHeader>
      {/* @ts-ignore */}
      <CardBody className="">
        <button className="text-blue-gray-900 transition-colors hover:text-gray-800">
          {/* @ts-ignore */}
          <Typography variant="h5" className="mb-2">
            {title}
          </Typography>
        </button>
        {/* @ts-ignore */}
        <Typography className="mb-6 font-normal !text-gray-500">
          {desc}
        </Typography>
        <div className="flex flex-col items-start gap-2">
          <div className="flex gap-2">
            {/* @ts-ignore */}
            <Button
              disabled={!isDemoAvailable}
              color="gray"
              className="capitalize disabled:opacity-35"
              size="sm"
              // onClick={() => (window.location.href = demo!)}
              onClick={() => window.open(demo, "_blank")}
            >
              View Demo
            </Button>
            {isBlogAvailable && (
              <Link href={blog!}>
                {/*@ts-ignore */}
                <Button variant="outlined" className="py-2 px-3 capitalize">
                  View Blog
                </Button>
              </Link>
            )}
            {isCodeAvailable && (
              //@ts-ignore
              <Button
                onClick={() => window.open(code, "_blank")}
                variant="outlined"
                className="py-2 px-3 capitalize"
              >
                View Code
              </Button>
            )}
          </div>
          {title === "App Orchestrator" && (
            // @ts-ignore
            <Typography className="font-normal !text-gray-500">
              <b>Username</b>: john@example.com, <b>Password</b>: 123
            </Typography>
          )}

          {!isDemoAvailable && (
            // @ts-ignore
            <Typography className="font-normal !text-gray-500">
              Sorry no demo available!!!
            </Typography>
          )}
        </div>
      </CardBody>
    </Card>
  );
}

export default ProjectCard;

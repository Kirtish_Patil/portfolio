import { Typography, Button } from "@material-tailwind/react";

// const LINKS = ["Skills", "Projects", "Articles", "Contact"];
const LINKS = [
  { label: "Skills", href: "/#skills" },
  { label: "Projects", href: "/#projects" },
  { label: "Articles", href: "/articles" },
  { label: "Contact", href: "/#contact" },
];
const CURRENT_YEAR = new Date().getFullYear();

export function Footer() {
  return (
    <footer className="mt-10 px-8 pt-20">
      <div className="container mx-auto">
        <div className="mt-16 flex flex-wrap items-center justify-center gap-y-4 border-t border-gray-200 py-6 md:justify-between">
          {/* @ts-ignore */}
          <Typography className="text-center font-normal !text-gray-700">
            &copy; {CURRENT_YEAR} <a target="_blank">Kirtish Patil</a>
          </Typography>
          <ul className="flex gap-8 items-center">
            {LINKS.map((link) => (
              <li key={link.label}>
                {/* @ts-ignore */}
                <Typography
                  as="a"
                  href={link.href}
                  variant="small"
                  className="font-normal text-gray-700 hover:text-gray-900 transition-colors"
                >
                  {link.label}
                </Typography>
              </li>
            ))}
            {/* @ts-ignore */}
          </ul>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

// @ts-nocheck
import React from "react";
import {
  Navbar as MTNavbar,
  Collapse,
  Button,
  IconButton,
  Typography,
  iconButton,
} from "@material-tailwind/react";
import {
  RectangleStackIcon,
  UserCircleIcon,
  CommandLineIcon,
  Squares2X2Icon,
  XMarkIcon,
  Bars3Icon,
} from "@heroicons/react/24/solid";
import { FaLightbulb } from "react-icons/fa";
import { IoBarChart } from "react-icons/io5";
import { IoCall } from "react-icons/io5";

import { PiGlobeStandFill } from "react-icons/pi";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";

const NAV_MENU = [
  {
    name: "Skills",
    id: "skills",
    href: "#skills",
    icon: FaLightbulb,
  },
  {
    name: "Projects",
    id: "projects",
    href: "#projects",
    icon: IoBarChart,
  },
  {
    name: "Articles",
    id: "articles",
    icon: PiGlobeStandFill,
    href: "/articles",
  },
  {
    name: "Contact",
    id: "contact",
    icon: IoCall,
    href: "#contact",
  },
];

interface NavItemProps {
  children: React.ReactNode;
  href?: string;
}

function NavItem({ children, href }: NavItemProps) {
  const router = useRouter();
  console.log("href", href);
  return (
    <Link href={href}>
      <Typography
        as="a"
        onPointerEnterCapture={() => {}}
        onPointerLeaveCapture={() => {}}
        variant="paragraph"
        color="gray"
        className="flex items-center gap-2 font-medium text-gray-900"
      >
        {children}
      </Typography>
    </Link>
  );
}

export function Navbar() {
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => setOpen((cur) => !cur);

  React.useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setOpen(false)
    );
  }, []);

  return (
    <MTNavbar
      shadow={false}
      fullWidth
      className="border-0 py-2 shadow-sm sticky top-0 z-50"
    >
      <div className="container mx-auto px-8 flex items-center justify-between">
        <Link href={"/#home"}>
          <Typography
            color="blue-gray"
            className="text-lg text-black font-bold"
          >
            Kirtish Patil
          </Typography>
        </Link>
        <ul className="ml-10 hidden items-center gap-8 lg:flex">
          {NAV_MENU.map(({ name, icon: Icon, href }) => (
            <NavItem key={name} href={href}>
              <Icon className="h-5 w-5" />
              {name}
            </NavItem>
          ))}
        </ul>
        <div className="hidden items-center rounded-full gap-2 lg:flex">
          <Link href={"/#home"}>
            <Image
              src={"/avatar/avatar2.png"}
              width={50}
              height={50}
              className="rounded-full"
            />
          </Link>
        </div>
        <IconButton
          variant="text"
          color="gray"
          onClick={handleOpen}
          className="flex items-center justify-center lg:hidden"
        >
          {open ? (
            <XMarkIcon strokeWidth={2} className="h-6 w-6" />
          ) : (
            <Bars3Icon strokeWidth={2} className="h-6 w-6" />
          )}
        </IconButton>
      </div>
      {open && (
        <Collapse open={open}>
          <div className="container mx-auto mt-3 border-t border-gray-200 px-2 py-2">
            <ul className="flex flex-col gap-4">
              {NAV_MENU.map(({ name, icon: Icon, href }) => (
                <NavItem key={name} href={href}>
                  <Icon className="h-5 w-5" />
                  {name}
                </NavItem>
              ))}
            </ul>
          </div>
        </Collapse>
      )}
    </MTNavbar>
  );
}

export default Navbar;

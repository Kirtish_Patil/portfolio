import { Card, CardBody, Typography } from "@material-tailwind/react";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "./Tooltip";

interface SkillCardProps {
  title: string;
  icons: { icon: React.ElementType; toolTip?: string }[];
  children: React.ReactNode;
}

export function SkillCard({ icons, title, children }: SkillCardProps) {
  return (
    // @ts-ignore
    <Card color="transparent" shadow={false}>
      {/* @ts-ignore */}
      <CardBody className="grid justify-center text-center">
        <div className="flex gap-2 justify-center">
          {icons.map(({ icon: Icon, toolTip }, idx) => (
            <>
              <TooltipProvider>
                <Tooltip delayDuration={0}>
                  <TooltipTrigger>
                    <button
                      data-status={"false"}
                      className="mb-6 grid h-12 w-12 place-items-center rounded-full p-2.5 shadow"
                    >
                      <Icon className="size-7" />
                    </button>
                  </TooltipTrigger>
                  <TooltipContent>
                    <p>{toolTip}</p>
                  </TooltipContent>
                </Tooltip>
              </TooltipProvider>
            </>
          ))}
        </div>
        {/* @ts-ignore */}
        <Typography variant="h5" color="blue-gray" className="mb-2">
          {title}
        </Typography>
        {/* @ts-ignore */}
        <Typography className="px-8 font-normal !text-gray-500">
          {children}
        </Typography>
      </CardBody>
    </Card>
  );
}

export default SkillCard;

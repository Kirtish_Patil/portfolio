import React, { FC, ReactNode } from "react";

const BlogHeading: FC<{ children: ReactNode; id: string }> = ({
  children,
  id,
}) => {
  return (
    <div id={id} className="scroll-mt-20">
      {children}
    </div>
  );
};

// Gitlab Api token - Go to you gitlab Account, inside you project, go to Settings > Api Token. Generate a new token by giving checking api access and naming it as 'gitlab-api-token'. Inside Jenkins Credentials, from the dropdown select Gitlab Api Token paste the token and give it the same id.

export default BlogHeading;

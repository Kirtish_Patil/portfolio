import { SiTypescript } from "react-icons/si";
import React, { FC, SVGAttributes } from "react";
import cn from "classnames";

const TypescriptIcon: FC<SVGAttributes<SVGElement>> = (props) => {
  return <SiTypescript {...props} className="size-7 text-[#3178c6]" />;
};

export default TypescriptIcon;

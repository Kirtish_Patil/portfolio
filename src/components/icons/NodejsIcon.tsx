import React, { FC, SVGAttributes } from "react";
import cn from "classnames";
import { FaNodeJs } from "react-icons/fa";

const NodejsIcon: FC<SVGAttributes<SVGElement>> = (props) => {
  return <FaNodeJs {...props} className="size-7 text-lime-600" stroke="2" />;
};

export default NodejsIcon;

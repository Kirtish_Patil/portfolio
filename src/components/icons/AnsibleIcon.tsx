import React, { FC, SVGAttributes } from "react";
import cn from "classnames";
import { SiAnsible } from "react-icons/si";

const AnsibleIcon: FC<SVGAttributes<SVGElement>> = (props) => {
  return <SiAnsible className={cn(props.className, "size-10")} {...props} />;
};

export default AnsibleIcon;
